Source: ancient
Section: utils
Priority: optional
Maintainer: Alex Myczko <tar@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 autoconf-archive,
 pkgconf,
Standards-Version: 4.7.0
Homepage: https://github.com/temisu/ancient
Vcs-Browser: https://salsa.debian.org/tar/ancient
Vcs-Git: https://salsa.debian.org/tar/ancient.git
Rules-Requires-Root: no

Package: ancient
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, , libancient2 (= ${binary:Version})
Description: decompression routines for ancient formats
 This is a collection of decompression routines for old formats popular in the
 Amiga, Atari computers and some other systems from 80's and 90's as well as
 some that are currently used which were used in a some specific way in these
 old systems.
 .
 For simple usage both a simple command line application as well as a simple
 API to use the decompressors are provided. The compression algorithm is
 automatically detected in most cases, however there are some corner cases
 where it is not entirely reliable due to weaknesses in the old format used.

Package: libancient-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libancient2 (= ${binary:Version})
Description: decompression routines for ancient formats - development files
 This is a collection of decompression routines for old formats popular in the
 Amiga, Atari computers and some other systems from 80's and 90's as well as
 some that are currently used which were used in a some specific way in these
 old systems.
 .
 For simple usage both a simple command line application as well as a simple
 API to use the decompressors are provided. The compression algorithm is
 automatically detected in most cases, however there are some corner cases
 where it is not entirely reliable due to weaknesses in the old format used.
 .
 This package provides ancient library development files.

Package: libancient2
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: decompression routines for ancient formats
 This is a collection of decompression routines for old formats popular in the
 Amiga, Atari computers and some other systems from 80's and 90's as well as
 some that are currently used which were used in a some specific way in these
 old systems.
 .
 For simple usage both a simple command line application as well as a simple
 API to use the decompressors are provided. The compression algorithm is
 automatically detected in most cases, however there are some corner cases
 where it is not entirely reliable due to weaknesses in the old format used.
